import React, { useState, useEffect } from 'react';
import styles from '../styles/Itemspage.module.css';
import { InputGroup, Form } from 'react-bootstrap';
import axios from 'axios';
import { Link, useLocation } from 'react-router-dom';

const ItemComponent = () => {

	//const URL = 'https://cs331e-379603.uc.r.appspot.com'
	let URL;
	if (window.location.hostname === 'localhost') {
		URL = 'http://127.0.0.1:5000/';
	} else {
		URL = 'https://cs331e-379603.uc.r.appspot.com';
	}

	const [data, setData] = useState(null);
	const [locations, setLocations] = useState([]);

	const [loading, setLoading] = useState(true);
	const [error, setError] = useState(false);
	const location = useLocation();
	const item = location.state.item;
	const [search, setSearch] = useState('');

	useEffect(() => {
		async function fetchData() {
			try {
				const dataResponse = await axios.get(`${URL}/stores`);
				const locationResponse = await axios.get(`${URL}/locations`);
				setData(dataResponse.data);
				setLocations(locationResponse.data);
				setLoading(false);
			} catch (error) {
				console.error('Error fetching data:', error);
				setLoading(false);
				setError(true);
			}
		}

		fetchData();


	}, []);

	if (loading) { return <div style={{ textAlign: 'center', fontSize: '20px' }}>Loading...</div>; }
	if (error) { return <div style={{ textAlign: 'center', fontSize: '20px' }}>Error fetching data...</div>; }

	const newData = data.map((data) => ({
		id: data[0],
		name: data[1],
		rating: data[2],
		count: data[3],
		img: data[4],
		items: data[5],
		location: data[6]
	}));

	const newLocations = locations.map((item) => ({
		id: item[0],
		name: item[1],
		capital: item[2],
		img: item[3],
		population: item[4],
		region: item[5],
		items: item[6],
		stores: item[7]
	}));

	const filteredStores = newData.filter(store => item.stores.includes(store.name));
	const filteredLocations = newLocations.filter(location => item.locations.includes(location.name))

	function capFirstLetter(string) {
		const words = string.split(' ');
		for (let i = 0; i < words.length; i++) {
			words[i] = words[i][0].toUpperCase() + words[i].substr(1);
		}
		return words.join(' ');
	}

	return (
		<div>
			{/* <!-- Title --> */}
			<div className={`w3-content ${styles.bodyContent}`} style={{ maxWidth: '2000px', marginTop: '50px' }}>
				<h1 className="w3-text-black" style={{
					textAlign: 'center', fontSize: '60px', textTransform: 'capitalize',
					fontWeight: "bold"
				}}>{item.name}</h1>
				<br />

				{/* <!-- Stores Section --> */}
				<div className="container overflow-hidden">
					<div>
						<Form>
							<InputGroup className='my-3'>
								{/* onChange for search */}
								<Form.Control
									onChange={(e) => setSearch(e.target.value)}
									placeholder={`Search stores and locations within ${capFirstLetter(item.name)}`}
								/>
							</InputGroup>
						</Form>
					</div>

					<div className={`${styles.row} mx-auto text-center`}>
						<div className="col" style={{ padding: 20 }}>
							<div className={`card text-center`} style={{ width: '23rem', height: '23rem' }}>
								<img src={item.img} className="card-img-top" style={{ height: '200px' }} alt={item.name} />
								<div className="card-body">
									<h4 className="card-title title" style={{ textTransform: 'capitalize', fontWeight: 'bold' }}>{item.name}</h4>
									<hr />
									<p className="card-text content">Quantity: {item.quantity}</p>
								</div>
							</div>
						</div>

						<div className="col" style={{ padding: 20 }}>
							<div className={`card text-center`} style={{ width: '23rem', height: '23rem' }}>
								<div className="card-body" style={{ overflowY: 'scroll' }}>
									<h4 className="card-title title" style={{ textTransform: 'capitalize', fontWeight: 'bold' }}>Available in these stores:</h4>
									<hr></hr>
									{
										filteredStores && filteredStores
											.filter((store) => {
												return search.toLowerCase() === ''
													? store.name
													: store.name.toLowerCase().includes(search.toLowerCase());
											}).map((store) => {
												return (
													<>
														<ul className="list-group" style={{ listStyleType: 'none', textTransform: 'capitalize' }}>
															<li>
																<Link to={`/store/${store.id}`} state={{ store }} style={{ textDecoration: "none", color: "black" }}>
																	<div className={`card ${styles.card}`} style={{ padding: 5 }}>
																		<p className="card-text content">{store.name}</p>
																	</div>
																</Link>
															</li>
														</ul>
													</>
												)
											})
									}
								</div>
							</div>
						</div>

						<div className="col" style={{ padding: 20 }}>
							<div className={`card text-center`} style={{ width: '23rem', height: '23rem' }}>
								<div className="card-body" style={{ overflowY: 'scroll' }}>
									<h4 className="card-title title" style={{ textTransform: 'capitalize', fontWeight: 'bold' }}>Available in these locations:</h4>
									<hr></hr>
									{
										filteredLocations && filteredLocations
											.filter((location) => {
												return search.toLowerCase() === ''
													? location.name
													: location.name.toLowerCase().includes(search.toLowerCase());
											}).map((location) => {
												return (
													<>
														<ul className="list-group" style={{ listStyleType: 'none', textTransform: 'capitalize' }}>
															<li>
																<Link to={`/location/${location.id}`} state={{ location }} style={{ textDecoration: "none", color: "black" }}>
																	<div className={`card ${styles.card}`} style={{ padding: 5 }}>
																		<p className="card-text content">{location.name}</p>
																	</div>
																</Link>
															</li>
														</ul>
													</>
												)
											})
									}
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>

			<footer className={`text-center text-white ${styles.myFooter}`} style={{ backgroundColor: '#fafafa' }}>
				<div className="text-center text-dark p-3" style={{ backgroundColor: 'rgba(154, 152, 152, 0.2)' }}>
					© 2023 Copyright: GroceryFinds
				</div>
			</footer>

		</div>

	);
};

export default ItemComponent;