import React from 'react';
import { Link } from 'react-router-dom';
import { Container, Row, Col } from 'react-bootstrap';
import logoFull from '../assets/logoFull.png';
import styles from '../styles/ModelsPage.module.css';
import strawberries from '../assets/strawberries.png';
import heb from '../assets/heb.png';
import austin from '../assets/austin.png';


const ModelsPage = () => {

    const models = [{ name: "Items", image: strawberries, link: "items" }, { name: "Stores", image: heb, link: "stores" }, { name: "Locations", image: austin, link: "locations" }];

    return (
        <div>
            {/* <!-- Page content --> */}
            <div id="page-container" className="w3-content" style={{ maxWidth: '2000px', marginTop: '46px' }}>
                <h1 className="w3-text-black" style={{ textAlign: 'center' }}>Models</h1>
                <hr className="center" />
                <br />

                {/* <!-- The main section --> */}
                <div className="container overflow-hidden text-center">
                    <div className={`${styles.row} mx-auto text-center`}>
                        {models.map(model => (
                            <div className="col">
                                <Link to={`/models/${model.link}`} style={{ "text-decoration": "none", color: "black" }}>
                                    <div className={`card ${styles.card}`} style={{ width: '23rem' }}>
                                        <img src={model.image} className="card-img-top" style={{ maxHeight: "170px" }} alt={model.image} />
                                        <div className="card-body">
                                            <h4 className="card-title title" style={{ "text-transform": "uppercase" }}>{model.name}</h4>
                                            <p className="card-text content" style={{ "font-size": "small", color: "dimgray" }}>Click for more information</p>
                                        </div>
                                    </div>
                                </Link>
                            </div>
                        ))}
                    </div>
                </div>
            </div>

            <footer className="text-center text-white" style={{ backgroundColor: '#fafafa' }}>
                <div className="text-center text-dark p-3" style={{ backgroundColor: 'rgba(154, 152, 152, 0.2)' }}>
                    © 2023 Copyright: GroceryFinds
                </div>
            </footer>
        </div>
    );
};

export default ModelsPage;