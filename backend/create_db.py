import json
from models import app, db, Item, Store, Location, item_stores, item_locations, store_locations

# ------------
# load_json
# ------------


def load_json(filename):
    """
    return a python dict jsn
    filename a json file
    """
    with open(filename) as file:
        jsn = json.load(file)
        file.close()

    return jsn


# ------------
# create_items
# ------------
def create_items():
    """
    populate item table
    """
    item = load_json('items_api.json')

    for oneItem in item['Items']:
        id = oneItem['id']
        name = oneItem['name']
        quantity = oneItem['quantity']
        image_url = oneItem['image_url']

        newItem = Item(id=id, name=name, quantity=quantity,
                       image_url=image_url)

        # After I create the book, I can then add it to my session.
        db.session.add(newItem)
        # commit the session to my DB.
        db.session.commit()


def create_stores():
    """
    populate store table
    """
    store = load_json('stores_api.json')

    for oneStore in store['Stores']:
        id = oneStore['id']
        name = oneStore['name']
        rating = oneStore['rating']
        store_count = oneStore['store_count']
        image_url = oneStore['image_url']

        newStore = Store(id=id, name=name, rating=rating,
                         store_count=store_count, image_url=image_url)

        db.session.add(newStore)

        db.session.commit()


def create_locations():
    """
    populate locations table
    """
    location = load_json('locations_api.json')

    for oneLocation in location['Locations']:
        id = oneLocation['id']
        name = oneLocation['name']
        capital = oneLocation['capital']
        image_url = oneLocation['image_url']
        population = oneLocation['population']
        region = oneLocation['region']

        newLocation = Location(id=id, name=name, capital=capital,
                               image_url=image_url, population=population, region=region)

        db.session.add(newLocation)

        db.session.commit()


def create_item_stores():
    """
    populate item_stores table
    """
    item = load_json('items_api.json')

    for oneItem in item['Items']:
        id = oneItem['id']
        queryItem = db.session.query(Item).filter(Item.id == id).first()
        item_stores_json = oneItem['item_stores']
        item_stores = []
        for store in item_stores_json:
            queryStore = db.session.query(
                Store).filter(Store.name == store).first()
            if queryStore:
                item_stores.append(queryStore)

        queryItem.item_stores = item_stores

        db.session.commit()


def create_item_locations():
    """
    populate item_locations table
    """
    item = load_json('items_api.json')

    for oneItem in item['Items']:
        id = oneItem['id']
        queryItem = db.session.query(Item).filter(Item.id == id).first()
        item_locations_json = oneItem['item_locations']
        item_locations = []
        for country in item_locations_json:
            queryLocation = db.session.query(
                Location).filter(Location.name == country).first()
            if queryLocation:
                item_locations.append(queryLocation)

        queryItem.item_locations = item_locations

        db.session.commit()


def create_store_locations():
    """
    populate store_locations table
    """
    store = load_json('stores_api.json')

    for oneStore in store['Stores']:
        id = oneStore['id']
        queryStore = db.session.query(Store).filter(Store.id == id).first()
        store_locations_json = oneStore['store_locations']
        store_locations = []
        for location in store_locations_json:
            queryLocation = db.session.query(
                Location).filter(Location.name == location).first()
            if queryLocation:
                store_locations.append(queryLocation)

        queryStore.store_locations = store_locations

        db.session.commit()


create_items()
create_stores()
create_locations()
create_item_stores()
create_item_locations()
create_store_locations()
