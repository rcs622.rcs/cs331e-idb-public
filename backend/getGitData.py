import requests
import sys

def get_gitlab_data(project_id, access_token):
    headers = {'Private-Token': access_token}
    totalCommit = 0
    totalIssue = 0

    commits_url = f'https://gitlab.com/api/v4/projects/{project_id}/repository/commits'
    commits = []
    page = 1
    while True:
        response = requests.get(commits_url, headers=headers, params={'page': page})
        data = response.json()
        if not data:
            break
        commits.extend(data)
        page += 1

    issues_url = f'https://gitlab.com/api/v4/projects/{project_id}/issues'
    issues = []
    page = 1
    while True:
        response = requests.get(issues_url, headers=headers, params={'page': page})
        data = response.json()
        if not data:
            break
        issues.extend(data)
        page += 1

    contributors = {}

    for commit in commits:
        author = commit['author_name']
        if author not in contributors:
            contributors[author] = {'commit_count': 0, 'issue_count': 0}
        contributors[author]['commit_count'] += 1

    for issue in issues:
        author = issue['author']['name']
        if author not in contributors:
            contributors[author] = {'commit_count': 0, 'issue_count': 0}
        contributors[author]['issue_count'] += 1

    for member in contributors:
        if member == "Shaojie Hou":
            contributors[member]['commit_count'] = contributors['shaojie']['commit_count']
        if member == "Yaashi Khatri":
            contributors[member]['commit_count'] = contributors['yaashi24']['commit_count']
    
    for member in contributors:
        totalCommit += contributors[member]['commit_count']
        totalIssue += contributors[member]['issue_count']
    
    contributors['totalCommit'] = totalCommit
    contributors['totalIssue'] = totalIssue
            

    
    return contributors
